---
title: "Deep Watching: New Methods for Analyzing Visual Media"
authors: "Bernhard Bermeitinger, Gernot Howanitz, Erik Radisch, Sebastian Gassner, Malte Rehbein, Siegfried Handschuh"
venue: "Exploring Audiovisual Composition — Images of Crisis"
date: "15.11.2019"
location: "Potsdam, Germany"
notes: "Presentation at a workshop"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/337276254)
