---
title: "Towards a Classification of Neoclassical Objects in Interior Scenes"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "Bilddaten in den Digitalen Geistesund Kulturwissenschaften – Interoperabilität und Retrieval"
location: "Darmstadt, Germany"
date: "October 2017"
notes: "Workshop Presentation"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/320346839)
