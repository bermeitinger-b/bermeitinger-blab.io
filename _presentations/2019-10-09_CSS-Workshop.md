---
title: "Representational Capacity of Deep Neural Networks: A Computing Study"
authors: "Bernhard Bermeitinger, Tomas Hrycej, Siegfried Handschuh"
venue: "12th Computational Social Science Workshop"
date: "09.10.2019"
location: "St.Gallen, Switzerland"
notes: "Presentation at a workshop"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/336374457)
