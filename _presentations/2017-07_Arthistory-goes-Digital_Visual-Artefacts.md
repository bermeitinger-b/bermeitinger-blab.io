---
title: "Visual Artefacts through the Black Box: Analysing Deep Learning Classifcation of Neoclassical Furniture Images"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "(Art-)History goes Digital"
location: "Munich, Germany"
date: "July 2017"
notes: "Workshop Presentation"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/319204814)
