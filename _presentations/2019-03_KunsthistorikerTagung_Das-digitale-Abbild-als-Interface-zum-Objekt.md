---
title: "Das digitale Abbild als Interface zum Objekt"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "XXXV. Deutscher Kunsthistorikertag 2019"
location: "Göttingen, Germany"
date: "March 2019"
notes: "Presentation"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/332344200)
