---
title: "Lernen mit Chatbots"
authors: "Bernhard Bermeitinger, Siegfried Handschuh"
venue: "Märztagung 2022 online: Lernwelten in der digitalen Transformation – heute und in Zukunft"
date: "24.03.2022"
location: "Visp, Switzerland"
notes: "Invited talk (German)"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/359437818)
