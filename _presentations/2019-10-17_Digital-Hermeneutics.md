---
title: "AI for the historical disciplines? Epistemological value, methodological challenges, perspectives and the transformation of research practices – the Neoclassica experience."
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "Digital Hermeneutics: From Research to Dissemination"
date: "17.10.2019"
location: "Washington DC, USA"
notes: "Presentation by Simon Donig"
---

[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/336995417)
