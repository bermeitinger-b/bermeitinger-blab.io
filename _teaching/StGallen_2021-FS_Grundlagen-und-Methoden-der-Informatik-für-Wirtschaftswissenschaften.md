---
title: "Grundlagen und Methoden der Informatik für Wirtschaftswissenschaftler"
location: "University of St.Gallen (HSG), St.Gallen, Switzerland"
role: "Teaching Assistant / Tutor and Coaching"
semester: "2021-02 – 2021-08 (FS)"
---
