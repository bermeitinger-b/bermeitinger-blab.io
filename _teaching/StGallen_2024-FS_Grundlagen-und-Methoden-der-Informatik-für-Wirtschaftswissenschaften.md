---
title: "Grundlagen und Methoden der Informatik für Wirtschaftswissenschaftler"
location: "University of St.Gallen (HSG), St.Gallen, Switzerland"
role: "Lecturer"
semester: "2024-02 – 2024-05 (FS)"
---
