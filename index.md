---
layout: default
---

## <a name="publications"> <i class="fa-solid fa-fw fa-newspaper"></i> Publications</a>

{% include publications.md %}

---

## <a name="presentations"> <i class="fa-solid fa-chalkboard-user"></i> Presentations</a>

{% include presentations.md %}

---

## <a name="pcs"> <i class="fa-solid fa-fw fa-gavel"></i> PCs</a>

{% include pcs.md %}

---

## <a name="teaching"> <i class="fa-solid fa-fw fa-school"></i> Teaching</a>

{% include teaching.md %}

---

## Impress

Dr. Bernhard Bermeitinger \\
Bruggwaldstrasse 103 \\
9300 Wittenbach, SG \\
Schweiz

research.bermeitinger@gmail.com
