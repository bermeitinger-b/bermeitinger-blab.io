<ul class="fa-ul">
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-twitter"></i></span> <a href="https://twitter.com/bermeitinger_b">@bermeitinger_b</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-mastodon"></i></span> <a rel="me" href="https://sigmoid.social/@bermeitinger_b">bermeitinger_b@sigmoid.social</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-researchgate"></i></span> <a href="https://www.researchgate.net/profile/Bernhard_Bermeitinger">ResearchGate</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-orcid"></i></span> <a href="https://orcid.org/0000-0002-2524-1850">ORCID: 0000-0002-2524-1850</a></li>
    <li><span class="fa-li"><i class="fa-solid fa-fw fa-swatchbook"></i></span> <a href="http://arxiv.org/a/bermeitinger_b_1">ArXiv</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-gitlab"></i></span> <a href="https://gitlab.com/bermeitinger-b">GitLab: bermeitinger-b</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-github"></i></span> <a href="https://github.com/bermeitinger-b">GitHub: bermeitinger-b</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-google-scholar"></i></span> <a href="https://scholar.google.com/citations?user=IvN2BsQyL-oC">Google Scholar</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-linkedin"></i></span> <a href="https://www.linkedin.com/in/bernhard-bermeitinger-a6aa2a117">LinkedIn</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-xing"></i></span> <a href="https://www.xing.com/profile/Bernhard_Bermeitinger">Xing</a></li>
    <li><span class="fa-li"><i class="fa-solid fa-fw fa-university"></i></span> <a href="https://www.alexandria.unisg.ch/persons/7816">Alexandria @ HSG</a></li>
    <li><span class="fa-li"><i class="fa-brands fa-fw fa-youtube"></i></span> <a href="https://www.youtube.com/channel/UCoPQC4Rx-tP71FL4gyMB1UQ">YouTube</a></li>
</ul>
