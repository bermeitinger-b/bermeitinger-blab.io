---
title: "IJCAI-PRICAI 2020"
long_title: "International Joint Conference on Artificial Intelligence -- Pacific Rim International Conference on Artificial Intelligence"
location: "Pacifico Yokohama, Japan _(Postponed due to Corona-Pandemic)_"
role: "Program Committee"
date: "January 2021"
---
