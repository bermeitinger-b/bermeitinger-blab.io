---
title: "ICANN2020"
long_title: "The 29th International Conference on Artificial Neural Networks."
location: "Bratislava, Slovakia _(Online due to Corona-Pandemic)_"
role: "Reviewer, ([Publication](https://www.springer.com/gp/book/9783030616083))"
date: "September 2020"
---
