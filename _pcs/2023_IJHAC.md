---
title: "IJHAC"
long_title: "International Journal of Humanities and Arts Computing"
location: "[Journal](https://www.euppublishing.com/loi/ijhac)"
role: "Peer Reviewer"
date: "October 2023"
---
