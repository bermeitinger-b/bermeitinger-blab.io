---
title: "How to Compute a Shape: Optical Character Recognition for Hieratic"
authors: "Bernhard Bermeitinger, Svenja A. Gülden, Tobias Konrad"
venue: "Handbook of Digital Egyptology: Texts (Monografías de Oriente Antiguo 1)"
location: "Gracia Zamacona, C. & J. Ortiz-García (eds.)"
date: "February 2022"
notes: "Alcalá de Henares: Editorial Universidad de Alcalá (pages 121-138), Antiguo"
---

[<i class="fa-solid fa-file-export"></i> book description](https://publicaciones.uah.es/export/sites/publicaciones/.galleries/Galeria-Servicio-de-Publicaciones/PDFs-Novedades/9788418979095W.pdf),
[<i class="fa-brands fa-researchgate"></i> chapter on ResearchGate](https://www.researchgate.net/publication/358467605)
