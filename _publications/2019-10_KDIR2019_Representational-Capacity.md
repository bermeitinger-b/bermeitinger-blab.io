---
title: "Representational Capacity of Deep Neural Networks: A Computing Study"
authors: "Bernhard Bermeitinger, Tomas Hrycej, Siegfried Handschuh"
venue: "KDIR2019, 11th International Conference on Knowledge Discovery and Information Retrieval"
location: "Vienna, Austria"
date: "October 2019"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> preprint](https://arxiv.org/abs/1907.08475) ([PDF](https://arxiv.org/pdf/1907.08475)),
[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/PublicationsDetail.aspx?ID=OnuZkDU3cF8%3d&t=1),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/336239891_Representational_Capacity_of_Deep_Neural_Networks_A_Computing_Study_Slides),
[<i class="fa-solid fa-book"></i> full proceedings](https://www.scitepress.org/ProceedingsDetails.aspx?ID=T4KTibRgTuo=&t=1)
