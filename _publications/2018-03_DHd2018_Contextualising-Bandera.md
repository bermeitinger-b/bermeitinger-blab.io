---
title: "Contextualising Bandera. Eine Distant Watching-Methode"
authors: "Bernhard Bermeitinger, Gernot Howanitz, Erik Radisch"
venue: 'DHd2018: Kritik der digitalen Vernunft, 5. Tagung des Verbands "Digital Humanities im deutschsprachigen Raum"'
location: "Cologne, Germany"
date: "March 2018"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> abstract](https://www.researchgate.net/publication/323507402),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://doi.org/10.13140/RG.2.2.22663.50084)
