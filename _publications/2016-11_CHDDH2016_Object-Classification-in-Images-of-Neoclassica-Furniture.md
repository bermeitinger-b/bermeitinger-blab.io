---
title: "Object Classification in Images of Neoclassical Furniture Using Deep Learning"
authors: "Bernhard Bermeitinger, André Freitas, Simon Donig, Siegfried Handschuh"
venue: "CHDDH 2016, Computational History and Data-Driven Humanities"
location: "Dublin, Ireland"
date: "November 2016"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://link.springer.com/chapter/10.1007/978-3-319-46224-0_10),
[<i class="fa-solid fa-book"></i> book](https://link.springer.com/book/10.1007/978-3-319-46224-0)
