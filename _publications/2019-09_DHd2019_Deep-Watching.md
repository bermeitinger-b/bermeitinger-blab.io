---
title: "Deep Watching: Towards New Methods of Analyzing Visual Media in Cultural Studies"
authors: "Bernhard Bermeitinger, Sebastian Gassner, Siegfried Handschuh, Gernot Howanitz, Erik Radisch, Malte Rehbein"
venue: "DH2019, Digital Humanities 2019: Complexities"
location: "Utrecht, Netherlands"
date: "July 2019"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://doi.org/10.5281/zenodo.8224858),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://zenodo.org/record/3326470)
