---
title: "Reducing the Transformer Architecture to a Minimum"
authors: "Bernhard Bermeitinger, Tomas Hrycej, Massimo Pavone, Julianus Kath, Siegfried Handschuh"
venue: "KDIR2024, 16th International Conference on Knowledge Discovery and Information Retrieval"
location: "Porto, Portugal"
date: "November 2024"
notes: "Poster Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/PublicationsDetail.aspx?ID=REzv8Neq7FY=&t=1),
[<i class="fa-solid fa-file-powerpoint"></i> poster on ResearchGate](https://www.researchgate.net/publication/386044867),
[<i class="fa-brands fa-researchgate"></i> paper on ResearchGate](https://www.researchgate.net/publication/386025529),
[<i class="fa-solid fa-file-pdf"></i> preprint]({{ "/assets/pdfs/2024-10 - Reducing the Transformer Architecture to a Minimum - Bermeitinger et al.pdf" | prepend: site.baseurl }}),
[<i class="fa-solid fa-file-lines"></i> preprint on ArXiv](https://arxiv.org/abs/2410.13732),
[<i class="fa-brands fa-researchgate"></i> preprint on ResearchGate](https://www.researchgate.net/publication/385009981)
