---
title: "FinBERT-FOMC: Fine-Tuned FinBERT Model with Sentiment Focus Method for Enhancing Sentiment Analysis of FOMC Minutes"
authors: "Sandro Gössi, Ziwei Chen, Wonseong Kim, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "ICAIF2023, 4th ACM International Conference on AI in Finance"
location: "New York, US"
date: "November 2023"
notes: "Poster Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://dl.acm.org/doi/10.1145/3604237.3626843),
[<i class="fa-solid fa-file-powerpoint"></i> poster](https://www.researchgate.net/publication/375999827),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/375920082)
