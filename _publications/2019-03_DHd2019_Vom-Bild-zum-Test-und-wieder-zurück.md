---
title: "Vom Bild zum Text und wieder zurück"
authors: "Bernhard Bermeitinger, Simon Donig, Maria Christoforaki, Siegfried Handschuh"
venue: 'DHd2019: multimedial & multimodal, 6. Tagung des Verbands "Digital Humanities im deutschsprachigen Raum"'
location: "Frankfurt (Main), Germany"
date: "March 2019"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.researchgate.net/publication/332275547),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/332441711VomBildzumTextundwiederzuruck),
[<i class="fa-solid fa-book"></i> book of abstracts](https://zenodo.org/record/2596095)
