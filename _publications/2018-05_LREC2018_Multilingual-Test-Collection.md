---
title: "A Multilingual Test Collection for the Semantic Search of Entity Categories"
authors: "Juliano Efson Sales, Siamak Barzegar, Wellington Franco, Bernhard Bermeitinger, Tiago Cunha, Brian Davis, André Freitas, Siegfried Handschuh"
venue: "LREC2018, Eleventh International Conference on Language Resources and Evaluation"
location: "Miyazaki, Japan"
date: "May 2018"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://aclanthology.org/L18-1398),
[<i class="fa-solid fa-researchgate"</i>> on ResearchGate](https://www.researchgate.net/publication/327728521)
