---
title: "Exploring the Usefulness of Open and Proprietary LLMs in Argumentative Writing Support"
authors: "Reto Gubelmann, Michael Burkhard, Rositsa Ivanova, Christina Niklaus, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "AIED2024, 25th International Conference on Artificial Intelligence in Education"
location: "Recife, Brazil"
date: "July 2024"
notes: "Presented by Rositsa Ivanova"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://link.springer.com/chapter/10.1007/978-3-031-64312-5_21),
[<i class="fa-brands fa-researchgate"></i> paper on ResearchGate](https://www.researchgate.net/publication/381901965)
