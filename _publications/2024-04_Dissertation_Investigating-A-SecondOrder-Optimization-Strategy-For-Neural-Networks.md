---
title: "Investigating a Second-Order Optimization Strategy for Neural Networks"
authors: "Bernhard Bermeitinger"
venue: "Universität Passau, Fakultät für Informatik und Mathematik"
location: "Passau, Germany"
date: "April 2024"
notes: "Published Dissertation (cumulative)"
---

[<i class="fa-solid fa-file-lines"></i> officially at Universität Passau](https://nbn-resolving.org/urn:nbn:de:bvb:739-opus4-14087),
[<i class="fa-solid fa-file-pdf"></i> on Zenodo](https://zenodo.org/records/11091505),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/380185812),
[<i class="fa-solid fa-file-pdf"></i> PDF]({{ "/assets/pdfs/2024-04 - Dissertation - Bermeitinger.pdf" | prepend: site.baseurl }}),
[<i class="fa-solid fa-file-powerpoint"></i> slides (Rigorosum) on Zenodo](https://zenodo.org/records/11091946),
[<i class="fa-solid fa-file-powerpoint"></i> slides (Rigorosum) on ResearchGate](https://www.researchgate.net/publication/380186265)
