---
title: "Object Classification in Images of Neoclassical Artifacts using Deep Learning"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, André Freitas, Siegfried Handschuh"
venue: "DH2017, Digital Humanities 2017"
location: "Montréal, Canada"
date: "August 2017"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> abstract](https://www.researchgate.net/publication/320413198),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/319174970)
