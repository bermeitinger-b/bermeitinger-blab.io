---
title: "Mathematical Foundations of Data Science"
authors: "Tomas Hrycej, Bernhard Bermeitinger, Matthias Cetto, Siegfried Handschuh"
venue: "Texts in Computer Science"
location: "Springer, Cham"
date: "April 2023"
notes: "Textbook"
---

[<i class="fa-solid fa-book"></i> on SpringerLink](https://link.springer.com/book/9783031190735),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/369218404)
