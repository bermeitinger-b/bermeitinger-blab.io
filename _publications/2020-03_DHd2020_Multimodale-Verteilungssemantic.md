---
title: "Multimodaler Bedeutungstransfer vom Text zum Bild. Granulare Bildklassifikation durch Verteilungssemantik."
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: 'DHd2020: Spielräume, Digital Humanities zwischen Modellierung und Interpretation, 7. Tagung des Verbands "Digital Humanities im deutschsprachigen Raum"'
location: "Paderborn, Germany"
date: "March 2020"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://zenodo.org/record/4621934),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/339800486),
[<i class="fa-solid fa-book"></i> full book of abstracts](https://zenodo.org/record/3666690),
[<i class="fa-solid fa-file-lines"></i> translated preprint (arXiv)](https://arxiv.org/abs/2001.02372)
