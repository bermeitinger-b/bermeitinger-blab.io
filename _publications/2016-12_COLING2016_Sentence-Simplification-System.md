---
title: "A Sentence Simplification System for Improving Relation Extraction"
authors: "Christina Niklaus, Bernhard Bermeitinger, André Freitas, Siegfried Handschuh"
venue: "COLING 2016, 26th International Conference on Computational Linguistics: System Demonstrations"
location: "Osaka, Japan"
date: "December 2016"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.researchgate.net/publication/315670222)
