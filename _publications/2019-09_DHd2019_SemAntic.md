---
title: "SemAntic: A Semantic Image Annotation Tool For The Humanities"
authors: "Simon Wagner, Simon Donig, Maria Christoforaki, Simon Donig, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "DH2019, Digital Humanities 2019: Complexities"
location: "Utrecht, Netherlands"
date: "July 2019"
notes: "Presented as Poster"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://doi.org/10.5281/zenodo.8225014),
[<i class="fa-solid fa-expand"></i> poster](https://www.researchgate.net/publication/334317882),
[<i class="fa-solid fa-file-code"></i> GitLab: University Passau](https://gitlab.com/nlp-passau/semantic-image-annotation-tool),
[<i class="fa-solid fa-file-code"></i> GitLab: University St.Gallen](https://gitlab.com/ds-unisg/semantic-image-annotation-tool)
