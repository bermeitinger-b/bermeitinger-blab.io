---
title: "Number of Attention Heads vs Number of Transformer-Encoders in Computer Vision"
authors: "Tomas Hrycej, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "KDIR2022, 14th International Joint Conference on Knowledge Discovery, Knowledge Engineering and Knowledge Management"
location: "Valetta, Malta"
date: "October 2022"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/PublicationsDetail.aspx?ID=8lZGKIPui9E=&t=1),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/364675154),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/364675146),
[<i class="fa-solid fa-file-lines"></i> preprint](https://arxiv.org/abs/2209.07221)
