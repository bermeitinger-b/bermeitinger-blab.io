---
title: "Efficient Neural Network Training via Subset Pretraining"
authors: "Jan Spörer, Bernhard Bermeitinger, Tomas Hrycej, Niklas Limacher, Siegfried Handschuh"
venue: "KDIR2024, 16th International Conference on Knowledge Discovery and Information Retrieval"
location: "Porto, Portugal"
date: "November 2024"
notes: "Paper Presentation (by Jan Spörer)"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/PublicationsDetail.aspx?ID=EvHJGVpxuVs=),
[<i class="fa-solid fa-file-powerpoint"></i> slides on ResearchGate](https://www.researchgate.net/publication/386044872),
[<i class="fa-brands fa-researchgate"></i> paper on ResearchGate](https://www.researchgate.net/publication/386024335),
[<i class="fa-solid fa-file-pdf"></i> preprint]({{ "/assets/pdfs/2024-10 - Efficient Neural Network Training via Subset Pretraining - Spörer et al.pdf" | prepend: site.baseurl }}),
[<i class="fa-solid fa-file-lines"></i> preprint on ArXiv](https://arxiv.org/abs/2410.16523),
[<i class="fa-brands fa-researchgate"></i> preprint on ResearchGate](https://www.researchgate.net/publication/385140740)
