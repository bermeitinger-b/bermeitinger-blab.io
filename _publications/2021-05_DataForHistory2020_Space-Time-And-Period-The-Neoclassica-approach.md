---
title: "Space, Time and Period - The Neoclassica Approach"
authors: "Simon Donig, Maria Christoforaki, Siegfried Handschuh"
venue: "Data for History 2020"
location: "Berlin, Germany _(online due to Corona)_"
date: "May 2021"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://d4h2020.sciencesconf.org/data/pages/Donig_Christoforaki_Handschuh_Space_time_and_period_1_.pdf), [<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/352063839)
