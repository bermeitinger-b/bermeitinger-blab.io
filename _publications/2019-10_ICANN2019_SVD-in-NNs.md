---
title: "Singular Value Decomposition and Neural Networks"
authors: "Bernhard Bermeitinger, Tomas Hrycej, Siegfried Handschuh"
venue: "ICANN2019, 28th International Conference on Artificial Neural Networks (Deep Learning)"
location: "Munich, Germany"
date: "October 2019"
notes: "Presented as Poster"
---

[<i class="fa-solid fa-file-lines"></i> preprint](https://arxiv.org/abs/1906.11755) ([PDF](https://arxiv.org/pdf/1906.11755)),
[<i class="fa-solid fa-file-lines"></i> paper](https://link.springer.com/chapter/10.1007%2F978-3-030-30484-3_13),
[<i class="fa-solid fa-expand"></i> poster](https://www.researchgate.net/publication/335883193_Singular_Value_Decomposition_and_Neural_Networks_Poster),
[<i class="fa-solid fa-book"></i> full proceedings](https://link.springer.com/book/10.1007/978-3-030-30484-3)
