---
title: "Make Deep Networks Shallow Again"
authors: "Bernhard Bermeitinger, Tomas Hrycej, Siegfried Handschuh"
venue: "KDIR2023, 15th International Conference on Knowledge Discovery and Information Retrieval"
location: "Rome, Italy"
date: "November 2023"
notes: "Paper Presentation, Nominated for Best Student Paper"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/Link.aspx?doi=10.5220/0012203800003598),
[<i class="fa-solid fa-file-powerpoint"></i> slides on ResearchGate](https://www.researchgate.net/publication/375824880),
[<i class="fa-brands fa-researchgate"></i> paper on ResearchGate](https://www.researchgate.net/publication/375803600),
[<i class="fa-solid fa-file-pdf"></i> preprint]({{ "/assets/pdfs/2023-09 - Make Deep Networks Shallow Again - Bermeitinger et al.pdf" | prepend: site.baseurl }}),
[<i class="fa-solid fa-file-lines"></i> preprint on ArXiv](https://dx.doi.org/10.48550/arXiv.2309.08414),
[<i class="fa-brands fa-researchgate"></i> preprint on ResearchGate](https://www.researchgate.net/publication/373992877)
