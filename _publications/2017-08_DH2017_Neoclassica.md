---
title: "Neoclassica – An Open Framework for Research in Neoclassicism"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "DH2017, Digital Humanities 2017"
location: "Montréal, Canada"
date: "August 2017"
notes: "Poster Presentation"
---

[<i class="fa-solid fa-file-lines"></i> abstract](https://www.researchgate.net/publication/319307122)
