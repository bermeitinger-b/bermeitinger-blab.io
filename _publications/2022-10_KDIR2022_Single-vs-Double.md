---
title: "Training Neural Networks in Single vs Double Precision"
authors: "Tomas Hrycej, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "KDIR2022, 14th International Joint Conference on Knowledge Discovery, Knowledge Engineering and Knowledge Management"
location: "Valetta, Malta"
date: "October 2022"
notes: "Paper Presentation"
---

[<i class="fa-solid fa-file-lines"></i> paper](https://www.scitepress.org/Link.aspx?doi=10.5220/0011577900003335),
[<i class="fa-solid fa-file-powerpoint"></i> slides](https://www.researchgate.net/publication/364675235),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/364674985),
[<i class="fa-solid fa-file-lines"></i> preprint](https://arxiv.org/abs/2209.07219)
