---
title: "Towards a Classification of Neoclassical Objects in Interior Scenes"
authors: "Simon Donig, Maria Christoforaki, Bernhard Bermeitinger, Siegfried Handschuh"
venue: "Bilddaten in den Digitalen Geisteswissenschaften"
location: "Canan Hastik, Philipp Hegel"
date: "November 2020"
notes: "Book Chapter, DOI [10.13173/9783447114608.149](https://www.doi.org/10.13173/9783447114608.149)"
---

[<i class="fa-solid fa-file-lines"></i> chapter](https://www.doi.org/10.13173/9783447114608.149),
[<i class="fa-brands fa-researchgate"></i> on ResearchGate](https://www.researchgate.net/publication/344792905)
